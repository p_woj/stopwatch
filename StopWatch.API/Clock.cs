﻿using System;
using System.Timers;

namespace StopWatch.API
{
    public class Clock
    {
        string time;

        private int _seconds;
        private int _minutes;
        private int _hours;

        private Timer _timer;

        public Clock()
        {
            _seconds = 0;
            _minutes = 0;
            _hours = 0;

            time = _hours.ToString("00") + _minutes.ToString("00") + _seconds.ToString("00");
            _timer = new Timer();
            _timer.Interval = 1000;
            _timer.Elapsed += _timer_Elapsed;
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            IncrementTime();
        }

        private void IncrementTime()
        {
            _seconds++;
            if (_seconds == 60)
            {
                _seconds = 0;
                _minutes++;
            }
            if (_minutes == 60)
            {
                _minutes = 0;
                _hours++;

            }
        }

        public string GetTime()
        {
            return _hours.ToString("00") +":"+ _minutes.ToString("00") +":"+ _seconds.ToString("00");
        }

        public void Start()
        {
            _timer.Start();
        }

        public void Pause()
        {
            _timer.Stop();
        }

        public void SetTime(int second, int minutes, int hours)
        {
            _seconds = second;
            _minutes = minutes;
            _hours = hours;
        }

        public int GetSeconds()
        {
            return _seconds;
        }
    }
}
