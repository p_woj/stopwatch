﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StopWatch.API;
using System.Threading;

namespace StopWatch.Tests
{
    [TestClass]
    public class ClockTests
    {
        private Clock _clock;

        [TestInitialize]
        public void SetUp()
        {
            _clock = new Clock();
        }

        [TestMethod]
        public void ClockShouldDisplayZeroTimeAfterInitializedTest()
        {
            var time = _clock.GetTime();

            Assert.IsNotNull(time);
            Assert.AreEqual("00:00:00", time);
        }

        [TestMethod]
        public void ClockChangeItsValueWhenTimePassesTest()
        {
            var initialTime = _clock.GetTime();
            _clock.Start();
            Thread.Sleep(1200);
            _clock.Pause();
            var timeAfter = _clock.GetTime();

            Assert.AreEqual(timeAfter, "00:00:01");
        }

        [TestMethod]
        public void ClockIsNotRunningAnymoreWhenStoppedTest()
        {
            _clock.Start();
            _clock.Pause();
            var timeBefore = _clock.GetTime();
            Thread.Sleep(1200);
            var timeAfter = _clock.GetTime();

            Assert.AreEqual(timeBefore, timeAfter);
        }

        [TestMethod]
        public void TimeActuallyPassesTest()
        {
            _clock.SetTime(46,0,0);
            var timeBefore = _clock.GetSeconds();
            _clock.Start();
            Thread.Sleep(1200);
            _clock.Pause();
            var timeAfter = _clock.GetSeconds();

            Assert.IsTrue(timeAfter > timeBefore);
        }

        [TestMethod]
        public void DisplaysMinutesCorrectlyTest()
        {
            _clock.SetTime(59,0,0);
            _clock.Start();
            Thread.Sleep(1200);
            _clock.Pause();
            var time = _clock.GetTime();

            Assert.AreEqual("00:01:00", time);
        }

        [TestMethod]
        public void DisplaysHoursCorrectlyTest()
        {
            _clock.SetTime(59, 59, 0);
            _clock.Start();
            Thread.Sleep(1200);
            _clock.Pause();
            var time = _clock.GetTime();

            Assert.AreEqual("01:00:00", time);
        }
    }
}
